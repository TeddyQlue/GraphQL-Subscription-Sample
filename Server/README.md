## How to run
```
npm i
npm start
```

## Purpose
This is a repository dedicated to show how to implement GraphQL Subscription with Apollo on Express.js Server.

## Setup For no client testing

Open Graphiql interface and Subscribe to messageSub.
```
localhost:3000/graphiql
```
```
#subscribed to channel 1
subscription ($message: MessageInput!) {
  messageSub(message:$message){
    channelId
    text
  }
}

#variables, currently unused.
{
	"message": {
		"channelId": 1,
 	  "text": "test text, unused"
	}
}
```
The response should be below if the subscription is successful.
```
"Your subscription data will appear here after server publication!"
```
otherwise you may need to check browser devtools => network => ws
to search for websocket logs.

Next setup the request from Insomnia
```
# Request Syntax
mutation ($message: MessageInput!){
	messageAdd(message: $message){
		channelId
		text
	}
}

#Variables
{
	"message": {
		"channelId": 1,
 	  "text": "some message you want to test :v"
	}
}
```
On both Insomnia and GraphiQL should return / received the same data;
```
{
	"data": {
		"messageAdd": {
			"channelId": "1",
			"text": "teddy"
		}
	}
}
```

## Dataloader Test

On the server's /graphiql endpoint you inspect example query differences
between normal query and dataloader approach on the server's console.

With normal resolver:
```
query {
  getPerson {
    name
    bestFriend {
      name
    }
  }
}
```

With data loader:
```
query {
  getPersonDataLoader {
    name
    bestFriend {
      name
    }
  }
}
```

Each console log represents a resolver attempting to request data from a source like Database.

Any further repeated request from dataloader query endpoint will not request the data further as the data is already cached.
