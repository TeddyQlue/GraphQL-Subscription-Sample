const fs = require('fs')

const subscriptionSchema = fs.readFileSync(__dirname + '/base.gql', 'utf-8')

module.exports = subscriptionSchema;
