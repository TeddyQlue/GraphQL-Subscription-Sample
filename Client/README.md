## How to run
```
npm i
npm start
```

## Purpose
This is a repository dedicated to show how to implement GraphQL Subscription with Apollo in React.

Access locally on two different browser window to view the effects of live subsription.
```
http://localhost:4000/
```
