import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

/****************************
*    GRAPHQL COMPONENTS     *
****************************/
import MessageGet from './components/messageQuery.js'
import MessageMutate from './components/messageMutation.js'
import MessageSubscribe from './components/messageSubscription.js'

/******************************
*    APP SCREEN COMPONENT     *
******************************/
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <MessageGet />
        <p className="App-intro">
          Start chatting!
        </p>
        <MessageMutate />
        <MessageSubscribe />
      </div>
    );
  }
}

export default App;
