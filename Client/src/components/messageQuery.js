import React, { Component } from 'react';
import { Query } from "react-apollo";

import { messageQuery, messageSubs } from '../graphql/'

/*************************************************
*   Child Component - allows for subscribing     *
*************************************************/
class PostList extends Component {
  componentDidMount () {
    this.props.subscribe()
  }

  render () {
    if (this.props.loading) return <p>Loading...</p>;
    if (this.props.error) return <p>Error :(</p>;
    return this.props.data.messageGet.map(({ text, channelId }, index) => (
      <div key={index}>
        <p>{`channel ${channelId}: ${text}`}</p>
      </div>
    ));
  }
}

/****************************************************************
*    Main Query Component - prop data and subs to child comp    *
****************************************************************/
let variableSub = {
  message: {
    channelId: 1,
    text: 'test'
  }
}

const MessageGet = () => (
  <Query
    query={messageQuery}
  >
    {({ loading, error, data, subscribeToMore }) => {
      return (
        <PostList
          data={data}
          loading={loading}
          error={error}
          subscribe={() => {
            console.log('subbing...');
            subscribeToMore({
              document: messageSubs,
              variables: variableSub,
              updateQuery: (prev, { subscriptionData }) => {
                // console.log(prev);
                if (!subscriptionData.data) return prev;
                const newFeedItem = subscriptionData.data.messageSub;

                // VALIDATION - CHECK FOR DUPES - IDEALLY USING UNIQUE IDs
                let dupeCheck = prev.messageGet.some((el, index, arr) => {
                  return el.text === newFeedItem.text && el.channelId === newFeedItem.channelId
                })
                if (!dupeCheck) {
                  // console.log(dupeCheck);
                  // Create new object with previous and new data --- The object is immutable
                  let newData = Object.assign({}, prev, {
                    messageGet: [...prev.messageGet, newFeedItem]
                  })
                  // console.log(newData);
                  return newData
                } else {
                  return prev
                }

              }
            })
          }}

        />
      );
    }}
  </Query>
)

export default MessageGet
