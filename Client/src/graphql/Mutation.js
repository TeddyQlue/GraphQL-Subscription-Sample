import gql from "graphql-tag";

export const messageAdd = gql`
  mutation ($message: MessageInput!){
    messageAdd(message: $message){
      channelId
      text
    }
  }
`
